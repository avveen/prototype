# Prototype

## Instructions - Property Extraction
Before you can start property extraction, you must first adjust the `collect_paths.py` and `run_test.py` files. There are 3 places that start with `cmd =` which is then followed by a command from scion or the bwtester. Adjust this so it points to where you have scion and the bwtester.

Next, to target a specific AS, ensure it has an AS and a BWtester server running. Also, establish a mongodb client with a main database called `scionStatsDB` and a collection called `availableServers`. In `availableServers`, add all ASes that you wish to access. This must be in the format `source_address: "AS,[local ip]:port"`. For example, `source_address: "19-ffaa:1:10bd,[127.0.0.1]:30100". The local ip and port indicates where the bwtester is running.

Once this is all set up and you enabled your own AS, you can run the property extractor using `./test_suit.sh 1` where 1 indicates it runs for a single iteration.

## Instructions - Running the prototype
Once you have collected all paths and its properties, the prototype application itself can be started.

For the server/receiver: simply start the receiver.go and it will be ready to receive commands

For the client/controller: run `go run .` in the Controller folder to start it. You can then use the `help` command to get an overview of all commands you can use. There is already a policy file provided in the Controller folder you can use. Also, with a small edit in `controller.go` you can run the time test instead of the client program.


## Credits
The [bwtester](https://gitlab.science.ru.nl/avveen/prototype/-/tree/main/lib/bwtester) and the [pan](https://gitlab.science.ru.nl/avveen/prototype/-/tree/main/lib/pan) libraries are part of the [scion-apps](https://github.com/netsec-ethz/scion-apps) repository. We merely made a local import of it as originally the code was not compatible with the branch of scionproto we were using.

The [PathCollector](https://gitlab.science.ru.nl/avveen/prototype/-/tree/main/PathCollector) is a copy of [UPIN's](https://gitlab.science.ru.nl/avveen/prototype/-/tree/main/PathCollector) code with minor adjustments. Specifically, we adjusted some code to allow it to collect more paths, we lowered the MTU and bandwidth, and we had to adjust some file locations so it used the correct libraries.

