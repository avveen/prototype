package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"net/netip"
	"prototype/lib/pan"
	"time"
)

func runTest(address string) {
	var timesBase, timesPolicy, timesMulti []time.Duration
	for i := 0; i < 10; i++ {
		reset()
		dt := time.Now()
		testClientBase(address)
		timeNew := time.Now()
		timesBase = append(timesBase, timeNew.Sub(dt))
	}
	fmt.Println("Base times are: ")
	fmt.Println(timesBase)

	for i := 0; i < 10; i++ {
		reset()
		dt := time.Now()
		testClientPolicy(address)
		timeNew := time.Now()
		timesPolicy = append(timesPolicy, timeNew.Sub(dt))
	}
	fmt.Println("Policy times are: ")
	fmt.Println(timesPolicy)

	for i := 0; i < 10; i++ {
		reset()
		dt := time.Now()
		testClientMultipath(address)
		timeNew := time.Now()
		timesMulti = append(timesMulti, timeNew.Sub(dt))
	}
	fmt.Println("Multipath times are: ")
	fmt.Println(timesMulti)

}

func testClientBase(address string) error {
	addr, err := pan.ResolveUDPAddr(context.TODO(), address)
	if err != nil {
		return err
	}
	policy1, err := pan.PolicyFromCommandline("", "", false)
	Conn, err = pan.DialUDP(context.Background(), netip.AddrPort{}, addr, policy1, nil)
	if err != nil {
		return err
	}
	defer Conn.Close()

	return nil
}

func testClientPolicy(address string) error {
	addr, err := pan.ResolveUDPAddr(context.TODO(), address)
	if err != nil {
		return err
	}
	policy1, err := pan.PolicyFromCommandline("", "", false)
	Conn, err = pan.DialUDP(context.Background(), netip.AddrPort{}, addr, policy1, nil)
	if err != nil {
		return err
	}
	Client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		panic(err)
	}

	defer func() {
		if err = Client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	defer Conn.Close()

	command := "policy policy.json"
	commandHandler(command, Client, address)

	return nil
}

func testClientMultipath(address string) error {
	addr, err := pan.ResolveUDPAddr(context.TODO(), address)
	if err != nil {
		return err
	}
	policy1, err := pan.PolicyFromCommandline("", "", false)
	Conn, err = pan.DialUDP(context.Background(), netip.AddrPort{}, addr, policy1, nil)
	if err != nil {
		return err
	}
	Client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		panic(err)
	}

	defer func() {
		if err = Client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
	defer Conn.Close()

	command := "policy policy.json"
	commandHandler(command, Client, address)
	commandHandler("multipath true", Client, address)

	return nil
}

func reset() {
	MultipathEnabled = false
	PolicyLocation = ""
}
