package main

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"net/netip"
	"prototype/lib/helper"
	"prototype/lib/pan"
	"strings"
)

func changeConnectionOld(conn pan.Conn, address string, hops string) error {
	connTemp := conn
	policy, err := pan.PolicyFromCommandline(hops, "", false)
	addr, err := pan.ResolveUDPAddr(context.TODO(), address)
	if err != nil {
		return err
	}
	conn, err = pan.DialUDP(context.Background(), netip.AddrPort{}, addr, policy, nil)
	if err != nil {
		return err
	}
	err = connTemp.Close()
	if err != nil {
		return err
	}
	return nil
}

func changeConnection(Conn pan.Conn, hops string) (pan.Conn, error) {
	policy, err := pan.PolicyFromCommandline(hops, "", false)
	if err != nil {
		return nil, err
	}
	Conn.SetPolicy(policy)

	return Conn, nil
}

func findBestPossiblePath(client *mongo.Client, policyLocation string) helper.PathRank {
	pathstats := helper.RetrieveDocuments(client)
	scores := helper.RIM(pathstats, policyLocation)
	best := scores[0]
	for _, score := range scores {
		if score.PathRank > best.PathRank {
			best = score
		}
	}
	return best
}

func findSecondBestPath(client *mongo.Client, policyLocation string) helper.PathRank {
	pathstats := helper.RetrieveDocuments(client)
	scores := helper.RIM(pathstats, policyLocation)
	best := scores[0]
	secondbest := scores[1]
	for _, score := range scores {
		if score.PathRank > best.PathRank {
			secondbest = best
			best = score
		}
	}
	return secondbest
}

func multipath(Conn2 pan.Conn, address string, hops string) (pan.Conn, error) {
	newAddress := strings.Replace(address, "[127.0.0.1]:12345", "[127.0.0.2]:12346", -1)
	addr, err := pan.ResolveUDPAddr(context.TODO(), newAddress)
	if err != nil {
		return nil, err
	}
	policy1, err := pan.PolicyFromCommandline(hops, "", false)
	Conn2, err = pan.DialUDP(context.Background(), netip.AddrPort{}, addr, policy1, nil)
	if err != nil {
		return nil, err
	}

	return Conn2, nil
}
