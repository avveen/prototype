package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"net/netip"
	"os"
	"prototype/lib/helper"
	"prototype/lib/pan"
	"strconv"
	"strings"
	"time"
)

var PolicyLocation string
var MultipathEnabled bool
var Conn pan.Conn
var Conn2 pan.Conn
var coordx, coordy int64
var seq int64 //Sequence number for messages
var primaryPath, secondaryPath helper.PathRank

func main() {
	var err error

	remoteAddr := flag.String("remote", "", "[Controller] Remote (i.e. the server's) SCION Address (e.g. 17-ffaa:1:1,[127.0.0.1]:12345)")

	flag.Parse()

	//runTest(*remoteAddr) //To run the test
	err = runClient(*remoteAddr) //To run the Client
	check(err)
}

func runClient(address string) error {
	addr, err := pan.ResolveUDPAddr(context.TODO(), address)
	if err != nil {
		return err
	}
	policy1, err := pan.PolicyFromCommandline("", "", false)
	Conn, err = pan.DialUDP(context.Background(), netip.AddrPort{}, addr, policy1, nil)
	if err != nil {
		return err
	}
	defer Conn.Close()

	Client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		panic(err)
	}

	defer func() {
		if err = Client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	coordx = 0
	coordy = 0
	seq = 0

	for {
		fmt.Printf("Current coordinates are x: %d and y: %d\n", coordx, coordy)
		consoleReader := bufio.NewReader(os.Stdin)
		fmt.Print("> ")

		input, _ := consoleReader.ReadString('\n')

		//input = strings.ToLower(input)
		commandHandler(input, Client, address)

	}
	return nil
}

func commandHandler(input string, Client *mongo.Client, address string) {
	command := strings.Fields(input)
	if command == nil {
		invalidCmd()
		return
	}
	switch strings.ToLower(command[0]) {
	case "exit":
		fmt.Println("Good bye!")
		os.Exit(0)
	case "move":
		if len(command) != 3 {
			invalidCmd()
		}
		val, err := strconv.ParseInt(command[2], 10, 64)
		dir := strings.ToLower(command[1])
		if !dirChecker(dir) {
			fmt.Printf("Supplied value %s is not a valid direction\n", command[1])
		} else if err != nil {
			fmt.Printf("Supplied value %s is not a number\n", command[2])
		} else {
			moveInstruction(dir, val)
		}
	case "policy":
		if len(command) != 2 {
			fmt.Println("Please specify the policy path")
		} else {
			_, err := os.ReadFile(command[1])
			if err != nil {
				fmt.Println(command[1])
				fmt.Println(err)
				fmt.Println("Invalid file path")
			} else {
				PolicyLocation = command[1]
				primaryPath = findBestPossiblePath(Client, PolicyLocation)
				fmt.Printf("New path is %s\n", primaryPath.Hops)
				Conn, err = changeConnection(Conn, primaryPath.Hops)
				if err != nil {
					fmt.Println("Connection could not be changed. Please try again")
				}
				if MultipathEnabled {
					secondaryPath = findSecondBestPath(Client, PolicyLocation)
					Conn2, err = changeConnection(Conn2, secondaryPath.Hops)
					if err != nil {
						fmt.Println("Connection could not be changed. Please try again")
					}
				}
			}

		}
	case "multipath":
		if len(command) != 2 {
			fmt.Println("Please specify whether it must be enabled or disabled. Use true or false to do so.")
		} else if PolicyLocation == "" {
			fmt.Println("Please specify a policy first.")
		} else if strings.ToLower(command[1]) == "true" {
			if MultipathEnabled {
				fmt.Println("Multipathing was already enabled")
			} else {
				MultipathEnabled = true
				secondaryPath = findSecondBestPath(Client, PolicyLocation)
				err := errors.New("")
				Conn2, err = multipath(Conn2, address, secondaryPath.Hops)
				if err != nil {
					return
				}
				fmt.Printf("Multipathing is now enabled. The second path is %s\n", secondaryPath.Hops)
			}
		} else if strings.ToLower(command[1]) == "false" {
			if !MultipathEnabled {
				fmt.Println("Multipathing was already disabled")
			} else {
				MultipathEnabled = false
				err := Conn2.Close()
				if err != nil {
					return
				}
			}
		} else {
			fmt.Println("Invalid argument. Please use either true or false.")
		}
	case "status":
		if PolicyLocation == "" {
			fmt.Println("Status unknown. Please specify a policy first.")
		} else {
			fmt.Printf("Path %s status:\n", primaryPath.Hops)
			statusPrinter(primaryPath.PathRequirements)
			if MultipathEnabled {
				fmt.Printf("Path %s status:\n", secondaryPath.Hops)
				statusPrinter(secondaryPath.PathRequirements)
			}
		}

	case "help":
		printHelp()
	default:
		invalidCmd()
	}
}

func statusPrinter(stats []bool) {
	status := "Not all path requirements are met. The path does not meet the requirements for: "
	reqMet := true
	for i, stat := range stats {
		if !stat {
			reqMet = false
			status = status + numToProperty(i) + " "
		}
	}
	if reqMet {
		fmt.Println("All path requirements are met.")
	} else {
		fmt.Println(status)
	}
}

func numToProperty(i int) string {
	if i == 0 {
		return "latency"
	} else if i == 1 {
		return "bandwidth-up"
	} else if i == 2 {
		return "bandwidth-down"
	} else if i == 3 {
		return "geolocation"
	} else {
		return "invalid"
	}
}

func dirChecker(dir string) bool {
	directions := []string{"Up", "up", "Right", "right", "Down", "down", "Left", "left"}
	for _, el := range directions {
		if dir == el {
			return true
		}
	}
	return false
}

func processMove(dir string, amt int64) {
	switch dir {
	case "down":
		coordy = coordy - amt
	case "up":
		coordy = coordy + amt
	case "right":
		coordx = coordx + amt
	case "left":
		coordx = coordx - amt

	}
}

func moveInstruction(dir string, amt int64) {
	//var err2 error
	seqCurrent := seq
	nBytes, err := Conn.Write([]byte(fmt.Sprintf("%d: Move %s %d", seq, dir, amt)))
	if MultipathEnabled {
		go func() {
			_, err := Conn2.Write([]byte(fmt.Sprintf("%d: Move %s %d", seq, dir, amt)))
			if err != nil {
				fmt.Println(err)
			}
		}()
	}
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("Wrote %d bytes.\n", nBytes)
	if MultipathEnabled {
		go readDeadLineForConn2(dir, amt, seqCurrent)
	}
	buffer := make([]byte, 16*1024)
	if err = Conn.SetReadDeadline(time.Now().Add(1 * time.Second)); err != nil {
		fmt.Println(err)
	}
	n, err := Conn.Read(buffer)
	if errors.Is(err, os.ErrDeadlineExceeded) {
		return
	} else if err != nil {
		fmt.Println(err)
	}
	data := buffer[:n]
	if strings.Fields(string(data))[0] != "Successfully" {
		if seqCurrent == seq {
			processMove(dir, amt)
			seq += 1
		}

	}
	fmt.Printf("Received reply (Conn1): %s\n", data)
}

func readDeadLineForConn2(dir string, amt int64, seqCurrent int64) {
	buffer := make([]byte, 16*1024)
	var err error
	if err = Conn2.SetReadDeadline(time.Now().Add(1 * time.Second)); err != nil {
		fmt.Println(err)
	}
	n, err := Conn2.Read(buffer)
	if errors.Is(err, os.ErrDeadlineExceeded) {
		return
	} else if err != nil {
		fmt.Println(err)
	}
	data := buffer[:n]
	if strings.Fields(string(data))[0] != "Successfully" {
		if seqCurrent == seq {
			processMove(dir, amt)
			seq += 1
		}
	}
	fmt.Printf("Received reply (Conn2): %s\n", data)
}

func invalidCmd() {
	fmt.Println("Unfortunately, the command is invalid. Please try again. You can use `help` for more information on all commands.")
}

func printHelp() {
	fmt.Println("A PAN prototype\n\n" +
		"Available commands:\"" +
		"move <dir> <amt> \t\t send a direction to the receiver where <dir> is a cardinal direction (up, right, down, left) and <amt> is an integer value\n" +
		"addr <addr> \t\t set a new scion address to connect to\n" +
		"policy <name> \t\t where <name> is the file location of the policy file\n" +
		"multipath <true/false> \t\t if multipathing is enabled, the app will send data using the best two paths\n" +
		"status \t\t display current path status, specifically its score and whether it is compliant\n" +
		"help \t\t display this help information \n" +
		"exit \t\t exits the app")
}

// Check just ensures the error is nil, or complains and quits
func check(e error) {
	if e != nil {
		fmt.Fprintln(os.Stderr, "Fatal error:", e)
		os.Exit(1)
	}
}
