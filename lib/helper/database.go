package helper

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"regexp"
	"strconv"
)

const uri = "mongodb://localhost:27017/"

var results []PathTemp

type PathStat struct {
	Hops              string  `bson:"hops"`
	AvgLatency        float64 `bson:"avg_latency"`
	AvgBandwidthCs64  float64 `bson:"avg_bandwidth_cs_64"`
	AvgBandwidthSc64  float64 `bson:"avg_bandwidth_sc_64"`
	AvgBandwidthCsMtu float64 `bson:"avg_bandwidth_cs_MTU"`
	AvgBandwidthScMtu float64 `bson:"avg_bandwidth_sc_MTU"`
	IsolatedDomains   []int64 `bson:"isolated_domains"`
}

type PathTemp struct {
	ID                string   `bson:"_id,omitempty"`
	Hops              string   `bson:"hops,omitempty"`
	AvgLatency        string   `bson:"avg_latency,omitempty"`
	AvgBandwidthCs64  string   `bson:"avg_bandwidth_cs_64,omitempty"`
	AvgBandwidthSc64  string   `bson:"avg_bandwidth_sc_64,omitempty"`
	AvgBandwidthCsMtu string   `bson:"avg_bandwidth_cs_MTU,omitempty"`
	AvgBandwidthScMtu string   `bson:"avg_bandwidth_sc_MTU,omitempty"`
	IsolatedDomains   []string `bson:"isolated_domains,omitempty"`
}

func convertDocuments() []PathStat {
	var pathstats []PathStat

	for _, path := range results {
		latency, err := checkAndConvert(path.AvgLatency)
		if err != nil { //In case of invalid input
			break
		}
		avgBandwidthCs64, err := checkAndConvert(path.AvgBandwidthCs64)
		if err != nil { //In case of invalid input
			break
		}
		avgBandwidthSc64, err := checkAndConvert(path.AvgBandwidthSc64)
		if err != nil { //In case of invalid input
			break
		}
		avgBandwidthCsMtu, err := checkAndConvert(path.AvgBandwidthCsMtu)
		if err != nil { //In case of invalid input
			break
		}
		avgBandwidthScMtu, err := checkAndConvert(path.AvgBandwidthScMtu)
		if err != nil { //In case of invalid input
			break
		}
		var isolatedDomains []int64
		for _, isd := range path.IsolatedDomains {
			isolatedDomain, _ := strconv.ParseInt(isd, 10, 64)
			isolatedDomains = append(isolatedDomains, isolatedDomain)
		}

		res := PathStat{
			Hops:              path.Hops,
			AvgLatency:        latency,
			AvgBandwidthCs64:  avgBandwidthCs64,
			AvgBandwidthSc64:  avgBandwidthSc64,
			AvgBandwidthCsMtu: avgBandwidthCsMtu,
			AvgBandwidthScMtu: avgBandwidthScMtu,
			IsolatedDomains:   isolatedDomains,
		}

		pathstats = append(pathstats, res)
	}

	return pathstats

}

func checkAndConvert(str string) (float64, error) {
	re := regexp.MustCompile("[-+]?(?:\\d*\\.*\\d+)")
	res := re.FindAllString(str, -1)
	if len(res) > 0 {
		return strconv.ParseFloat(res[0], 64)
	} else {
		return -1, fmt.Errorf("error")
	}
}

func RetrieveDocuments(client *mongo.Client) []PathStat {
	pathCollection := client.Database("scionStatsDB").Collection("paths_stats")

	opts := options.Find().SetProjection(bson.D{{"hops", 1}, {"avg_latency", 1},
		{"avg_bandwidth_cs_64", 1}, {"avg_bandwidth_sc_64", 1},
		{"avg_bandwidth_cs_MTU", 1}, {"avg_bandwidth_sc_MTU", 1}, {"isolated_domains", 1}})
	//Somehow this stil includes the _id, does that get in the way from unmarshalling?

	cursor, err := pathCollection.Find(context.TODO(), bson.D{}, opts)
	// check for errors in the finding
	if err != nil {
		panic(err)
	}
	defer func(cursor *mongo.Cursor, ctx context.Context) {
		err := cursor.Close(ctx)
		if err != nil {

		}
	}(cursor, context.TODO())

	// check for errors in the conversion
	if err = cursor.All(context.TODO(), &results); err != nil {
		panic(err)
	}
	return convertDocuments()

}

// Old function to test mongo functionality. Moved to controller.
func mongoClient() {
	// Use the SetServerAPIOptions() method to set the Stable API version to 1
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	opts := options.Client().ApplyURI(uri).SetServerAPIOptions(serverAPI)

	// Create a new client and connect to the server
	client, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		panic(err)
	}

	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	// Send a ping to confirm a successful connection
	var result bson.M
	if err := client.Database("admin").RunCommand(context.TODO(), bson.D{{"ping", 1}}).Decode(&result); err != nil {
		panic(err)
	}
	fmt.Println("Pinged your deployment. You successfully connected to MongoDB!")
}
