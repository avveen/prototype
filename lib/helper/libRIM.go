package helper

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strconv"
	"strings"
)

type Property struct {
	Name     string  `json:"property"`
	RangeMin float64 `json:"rangeMin"`
	RangeMax float64 `json:"rangeMax"`
	IdealMin float64 `json:"idealMin"`
	IdealMax float64 `json:"idealMax"`
	Weight   float64 `json:"weight"`
	Info     string  `json:"additionalInfo"`
}

type PathRank struct {
	Hops string
	//AvgLatency       float64
	//AvgBandwidthUp   float64
	//AvgBandwidthDown float64
	//ISDscore         float64
	PathValues       []float64 //Order of values: Latency, Bandwidth up, Bandwidth down, ISD score
	PathRequirements []bool    //Order of values: Latency, Bandwidth up, Bandwidth down, ISD score
	PathRank         float64
}

func normalization(pathRanks []PathRank, Ranges [][]float64, Ideals [][]float64) []PathRank {
	for i, _ := range pathRanks {
		var newValues []float64
		var pathreqs []bool
		for j, val := range pathRanks[i].PathValues {
			rangeA := Ranges[j][0]
			rangeB := Ranges[j][1]
			rangeC := Ideals[j][0]
			rangeD := Ideals[j][1]
			dmin := math.Min(math.Abs(val-rangeC), math.Abs(val-rangeD))
			if val >= rangeC && val <= rangeD {
				newValues = append(newValues, 1)
				pathreqs = append(pathreqs, true)
			} else if val >= rangeA && val <= rangeC && rangeA != rangeC {
				norm := 1 - (dmin / math.Abs(rangeA-rangeC))
				newValues = append(newValues, norm)
				pathreqs = append(pathreqs, false)
			} else if val >= rangeD && val <= rangeB && rangeD != rangeB {
				norm := 1 - (dmin / math.Abs(rangeD-rangeB))
				newValues = append(newValues, norm)
				pathreqs = append(pathreqs, false)
			} else {
				fmt.Println("An Error occurred. Terminate function")
				return nil
			}
		}
		pathRanks[i].PathValues = newValues
		pathRanks[i].PathRequirements = pathreqs
	}
	return pathRanks

}

func applyWeights(pathRanks []PathRank, WEIGHTS []float64) []PathRank {
	for i, _ := range pathRanks {
		for j, _ := range pathRanks[i].PathValues {
			pathRanks[i].PathValues[j] *= WEIGHTS[j]
		}
	}

	return pathRanks
}

func relativeIndex(pathRanks []PathRank, WEIGHTS []float64) []PathRank {
	for i, _ := range pathRanks {
		iPlus := 0.0
		iMin := 0.0
		for j, val := range pathRanks[i].PathValues {
			iPlus = iPlus + math.Pow(val-WEIGHTS[j], 2)
			iMin = iMin + math.Pow(val, 2)
		}
		iPlus = math.Sqrt(iPlus)
		iMin = math.Sqrt(iMin)
		relInd := iMin / (iPlus + iMin)
		pathRanks[i].PathRank = relInd
	}

	return pathRanks
}

func parsejson(policyfile string) ([]float64, [][]float64, [][]float64, []Property) {
	jsonFile, err := os.Open(policyfile)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	type Properties struct {
		Property []Property `json:"properties"`
	}

	var properties Properties
	var returnProps []Property

	byteValue, _ := ioutil.ReadAll(jsonFile)
	_ = json.Unmarshal(byteValue, &properties)

	weights := []float64{0, 0, 0, 0}
	ranges := [][]float64{{0, 0}, {0, 0}, {0, 0}, {0, 0}}
	ideals := [][]float64{{0, 0}, {0, 0}, {0, 0}, {0, 0}}
	weightTot := 0.0

	for i := 0; i < len(properties.Property); i++ {
		property := properties.Property[i]
		weightTot += property.Weight
		var j int //To ensure the order is always correct even if the policy file puts the properties in a different order
		if property.Name == "latency" {
			j = 0
		}
		if property.Name == "bandwidthUp" {
			j = 1
		}
		if property.Name == "bandwidthDown" {
			j = 2
		}
		if property.Name == "geolocation" {
			j = 3
		}
		weights[j] = property.Weight
		ranges[j][0] = property.RangeMin
		ranges[j][1] = property.RangeMax
		ideals[j][0] = property.IdealMin
		ideals[j][1] = property.IdealMax

		returnProps = append(returnProps, property)
	}

	if weightTot != 1 {
		fmt.Println("WARNING: The weight total does not equal 1. Please check your policy file.")
	}

	return weights, ranges, ideals, returnProps

}

func minimalNonZero(x, y float64) float64 {
	if x < y && x != 0.00 {
		return x
	}
	return y
}

func MTUifNonZero(byteBandwidth, mtuBandwidth float64) float64 {
	if mtuBandwidth > 0 {
		return mtuBandwidth
	} else {
		return byteBandwidth
	}
}

func ISDconversion(isds []int64, propertiesISD string) float64 {
	words := strings.Fields(propertiesISD)
	blacklist := false //False while on whitelist. True when checking blacklist vals
	numOfISDs := len(isds)
	isdsCount := 0
	for _, word := range words {
		if strings.Contains(word, "Blacklist") {
			blacklist = true
		}
		word = strings.ReplaceAll(word, ",", "")
		isdFromProps, err := strconv.ParseInt(word, 10, 64)
		if err == nil { //Ensure it is a number, not the word whitelist or blacklist

			for i := range isds {
				if isds[i] == isdFromProps {
					if blacklist {
						return -1 //Encountered a blacklist ISD. The path is invalidated.
					}
					isdsCount++
				}
			}
		}
	}

	if isdsCount == numOfISDs {
		return 2 //All ISDs are in the list
	} else {
		return 1
	}
}

func preprocessing(input []PathStat, properties []Property) []PathRank {
	var paths []PathRank

	for _, path := range input {
		var lat, up, down, isd float64
		invalid := false
		for i := range properties {
			name := properties[i].Name
			if name == "latency" {
				lat = path.AvgLatency
				if lat > properties[i].RangeMax || lat < properties[i].RangeMin {
					invalid = true
				}
			}
			if name == "bandwidthUp" {
				up = MTUifNonZero(path.AvgBandwidthCs64, path.AvgBandwidthCsMtu)
				if up > properties[i].RangeMax || up < properties[i].RangeMin {
					invalid = true
				}
			}
			if name == "bandwidthDown" {
				down = MTUifNonZero(path.AvgBandwidthSc64, path.AvgBandwidthScMtu)
				if down > properties[i].RangeMax || down < properties[i].RangeMin {
					invalid = true
				}
			}
			if name == "geolocation" {
				isd = ISDconversion(path.IsolatedDomains, properties[i].Info)
				if isd > properties[i].RangeMax || isd < properties[i].RangeMin {
					invalid = true
				}
			}
		}
		if !invalid {
			pathrank := PathRank{
				Hops:             path.Hops,
				PathValues:       []float64{lat, up, down, isd},
				PathRequirements: []bool{true, true, true, true},
				PathRank:         0,
			}
			paths = append(paths, pathrank)
		}
	}

	return paths
}

func RIM(input []PathStat, policyfile string) []PathRank {
	weights, ranges, ideals, properties := parsejson(policyfile)

	processed := preprocessing(input, properties)

	if len(weights) != len(processed[0].PathValues) { //Check that number of properties is correct
		fmt.Println("ERROR: Number of path properties does not equal number of JSON properties.")
	}

	normalized := normalization(processed, ranges, ideals)
	weightsApplied := applyWeights(normalized, weights)
	relatives := relativeIndex(weightsApplied, weights)
	return relatives
}

/*
func main() { //a test function
	_, _, _, properties := parsejson("Controller/policy.json")

	path1 := PathStat{
		Hops:              "1-2-3-4",
		AvgLatency:        200,
		AvgBandwidthCs64:  200,
		AvgBandwidthSc64:  1000,
		AvgBandwidthCsMtu: 300,
		AvgBandwidthScMtu: 1000,
		IsolatedDomains:   []int64{17, 18},
	}
	path2 := PathStat{
		Hops:              "1-2-3-4",
		AvgLatency:        100,
		AvgBandwidthCs64:  250,
		AvgBandwidthSc64:  1200,
		AvgBandwidthCsMtu: 250,
		AvgBandwidthScMtu: 1600,
		IsolatedDomains:   []int64{16, 17, 18},
	}
	path3 := PathStat{
		Hops:              "1-2-3-4",
		AvgLatency:        300,
		AvgBandwidthCs64:  100,
		AvgBandwidthSc64:  1400,
		AvgBandwidthCsMtu: 100,
		AvgBandwidthScMtu: 1400,
		IsolatedDomains:   []int64{17, 18},
	}
	path4 := PathStat{
		Hops:              "1-2-3-4",
		AvgLatency:        400,
		AvgBandwidthCs64:  300,
		AvgBandwidthSc64:  1600,
		AvgBandwidthCsMtu: 300,
		AvgBandwidthScMtu: 1600,
		IsolatedDomains:   []int64{17, 18},
	}

	paths := []PathStat{path1, path2, path3, path4}

	processed := preprocessing(paths, properties)

	for _, process := range processed {
		fmt.Println(process)
	}
	relatives := rim(paths, "Controller/policy.json")

	fmt.Println(relatives)
}
*/
