package main

import (
	"context"
	"errors"
	"fmt"
	"net/netip"
	"os"
	"prototype/lib/pan"
	"strconv"
	"strings"
	"time"
)

var coordx, coordy int64
var seq int64 //Sequence number for messages

func main() {
	var err error
	//var listen pan.IPPortValue
	//flag.Var(&listen, "listen", "[Server] local IP:port to listen on")

	//flag.Parse()

	//fmt.Println(pan.ParseOptionalIPPort("127.0.0.1:1234"))
	port1, _ := netip.ParseAddrPort("127.0.0.1:12345")
	port2, _ := netip.ParseAddrPort("127.0.0.2:12346")

	//if listen.Get().Port() > 0 {
	//	check(fmt.Errorf("please specify -listen for server"))
	//}

	//err = runServer(listen.Get())
	go func() {
		err := runServer(port2)
		check(err)
	}()
	err = runServer(port1)
	check(err)
}

func verifyMessage(s string) error {
	received := strings.Fields(s)
	if strings.ToLower(received[0]) != "move" {
		return errors.New("invalid message")
	}
	dir := strings.ToLower(received[1])
	if dir != "down" && dir != "up" && dir != "right" && dir != "left" {
		return errors.New("invalid direction")
	}
	if _, err := strconv.ParseInt(received[2], 10, 64); err != nil {
		return errors.New("No valid number received")
	}

	return nil
}

func processMove(s string) {
	command := strings.Fields(s)
	numMove, _ := strconv.ParseInt(command[2], 10, 64)
	switch strings.ToLower(command[1]) {
	case "down":
		coordy = coordy - numMove
	case "up":
		coordy = coordy + numMove
	case "right":
		coordx = coordx + numMove
	case "left":
		coordx = coordx - numMove

	}
}

func getAndVerifySequence(s string) (string, int) {
	received := strings.Fields(s)
	lenSeq := len(received[0])
	seqReceived, err := strconv.ParseInt(received[0][:lenSeq-1], 10, 64)
	if err != nil {
		return s, 2
	}
	if seq == seqReceived {
		return s[lenSeq:], 0
	} else if seq == seqReceived+1 {
		return s[lenSeq:], 1
	} else {
		return s, 2
	}
}

func runServer(listen netip.AddrPort) error {
	conn, err := pan.ListenUDP(context.Background(), listen, nil)
	if err != nil {
		return err
	}
	defer conn.Close()
	fmt.Println(conn.LocalAddr())

	coordx = 0
	coordy = 0
	seq = 0

	buffer := make([]byte, 64*1024)
	for {
		fmt.Printf("Current coordinates are x: %d and y: %d\n", coordx, coordy)
		n, from, err := conn.ReadFrom(buffer)
		if err != nil {
			return err
		}
		data := buffer[:n]
		fmt.Printf("Received %s: %s\n", from, data)
		s, errNum := getAndVerifySequence(string(data))
		//fmt.Println(s) //REMOVE LATER, TEST PURPOSES
		if errNum == 2 {
			fmt.Println("Invalid sequence")
			continue
		}
		err = verifyMessage(s)
		var msg string
		if err != nil {
			fmt.Println(err)
			msg = fmt.Sprintf("Invalid command received %s", time.Now().Format("15:04:05.0"))
		} else {
			if errNum == 0 {
				processMove(s)
				seq += 1
				msg = fmt.Sprintf("Succesfully received %s", time.Now().Format("15:04:05.0"))
			} else {
				msg = fmt.Sprintf("Succesfully received duplicate message %s", time.Now().Format("15:04:05.0"))
			}
		}

		n, err = conn.WriteTo([]byte(msg), from)
		if err != nil {
			return err
		}
		fmt.Printf("Wrote %d bytes.\n", n)
	}
}

// Check just ensures the error is nil, or complains and quits
func check(e error) {
	if e != nil {
		fmt.Fprintln(os.Stderr, "Fatal error:", e)
		os.Exit(1)
	}
}
